package usermanager

const (
	MsgErrMissingFields = "Missing fields (needed password, email)."
	MsgErrCreateUser    = "Error creating user: %s"
	MsgErrNoIDGiven     = "No user id given."
	MsgErrIDNotANumber  = "ID is not a number!"

	MsgErrNotAuthorized    = "You are not authorized!"
	MsgErrConvertingUser   = "Error converting user object!"
	MsgErrAccessNotAllowed = "You don't have access to this resource!"
)
