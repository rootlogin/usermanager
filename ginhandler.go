package usermanager

import (
	"fmt"
	"github.com/badoux/checkmail"
	"github.com/jinzhu/gorm"
	"net/http"
	"regexp"
	"strconv"

	"github.com/gin-gonic/gin"
)

const (
	// Success messages
	MsgSucUserCreated = "User '%s' has been created!"

	// Regex
	RegExpLetter  = "[a-zA-Z]"
	RegExpNumber  = "[0-9]"
	RegExpSpecial = "[^A-Za-z0-9]"
)

type GinHandler struct {
	userManager    *UserManager
	authMiddleware *AuthMiddleware
}

func (gh *GinHandler) RegisterHandler(c *gin.Context) {
	// register data
	var registerData ApiReqRegister
	if c.BindJSON(&registerData) != nil {
		c.JSON(http.StatusBadRequest, apiResponse(MsgErrMissingFields))
		return
	}

	// validate data
	if valid, err := gh.validateNewUser(registerData); !valid {
		c.JSON(http.StatusBadRequest, apiResponse(err))
		return
	}

	// create user
	if _, err := gh.userManager.Create(registerData.Email, registerData.Password, registerData.DisplayName); err != nil {
		c.JSON(http.StatusInternalServerError, apiResponse(fmt.Sprintf(MsgErrCreateUser, err)))
		return
	}

	c.JSON(http.StatusCreated, apiResponse(fmt.Sprintf(MsgSucUserCreated, registerData.Email)))
}

func (gh *GinHandler) GetUserHandler(c *gin.Context) {
	// check if user is authenticated
	if !isUserAuthenticated(gh.authMiddleware, c) {
		return
	}

	// get user
	authUserObj, exists := c.Get("user")
	if !exists {
		c.AbortWithStatusJSON(http.StatusUnauthorized, apiResponse(MsgErrNotAuthorized))
		return
	}

	authUser, ok := authUserObj.(*User)
	if !ok {
		c.AbortWithStatusJSON(http.StatusInternalServerError, apiResponse(MsgErrConvertingUser))
		return
	}

	// retrieve id
	idStr := c.Param("id")
	if len(idStr) == 0 {
		c.AbortWithStatusJSON(http.StatusBadRequest, apiResponse(MsgErrNoIDGiven))
		return
	}

	id, err := strconv.Atoi(idStr)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, apiResponse(MsgErrIDNotANumber))
		return
	}

	// check if user is itself or has ROLE_ADMIN
	if !authUser.HasRole("ROLE_ADMIN") && authUser.ID != uint(id) {
		c.AbortWithStatusJSON(http.StatusForbidden, apiResponse(MsgErrAccessNotAllowed))
		return
	}

	// get user
	user, err := gh.userManager.Get(id)
	if err != nil {
		c.JSON(http.StatusInternalServerError, apiResponse(fmt.Sprintf(MsgErrCreateUser, err)))
		return
	}

	c.JSON(http.StatusOK, ApiResponse{
		Payload: user,
	})
}

func (gh *GinHandler) validateNewUser(user ApiReqRegister) (bool, string) {
	// display name checks
	if len(user.DisplayName) > 255 {
		return false, "Display name can have at maximum 255 chars."
	}

	// email checks
	if err := checkmail.ValidateFormat(user.Email); err != nil {
		return false, "Email address is not valid."
	}

	if len(user.Email) > 255 {
		return false, "Email address can have at maximum 255 chars."
	}

	if _, err := gh.userManager.GetByEmail(user.Email); err != gorm.ErrRecordNotFound {
		return false, "Email address is already registered."
	}

	// password checks
	if len(user.Password) < 8 {
		return false, "Password must have at least 8 chars."
	}

	if !gh.validatePassword(user.Password) {
		return false, "Password must have at least one letter, one number and one special character."
	}

	return true, ""
}

func (gh *GinHandler) validatePassword(password string) bool {
	letter, err := regexp.Compile(RegExpLetter)
	if err != nil {
		return false
	}

	number, err := regexp.Compile(RegExpNumber)
	if err != nil {
		return false
	}

	special, err := regexp.Compile(RegExpSpecial)
	if err != nil {
		return false
	}

	return letter.MatchString(password) && number.MatchString(password) && special.MatchString(password)
}

func apiResponse(message string) ApiResponse {
	return ApiResponse{Message: message}
}

func isUserAuthenticated(am *AuthMiddleware, c *gin.Context) bool {
	// check if user is authenticated
	authMiddleware := am.MiddlewareFunc()
	authMiddleware(c)

	// check if request was aborted by authentication middleware
	return !c.IsAborted()
}
