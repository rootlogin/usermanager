module gitlab.com/rootlogin/usermanager

go 1.12

require (
	github.com/badoux/checkmail v0.0.0-20181210160741-9661bd69e9ad
	github.com/denisenkom/go-mssqldb v0.0.0-20190515213511-eb9f6a1743f3 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible // indirect
	github.com/erikstmartin/go-testdb v0.0.0-20160219214506-8d10e4a1bae5 // indirect
	github.com/gin-gonic/gin v1.4.0
	github.com/jinzhu/gorm v1.9.4
	github.com/jinzhu/now v1.0.0 // indirect
	github.com/kr/pretty v0.1.0 // indirect
	github.com/lib/pq v1.1.1 // indirect
	github.com/nouney/randomstring v0.0.0-20180330205616-1374daa59f01 // indirect
	github.com/rs/zerolog v1.14.0
	golang.org/x/crypto v0.0.0-20190404164418-38d8ce5564a5
	golang.org/x/net v0.0.0-20190522155817-f3200d17e092 // indirect
	gopkg.in/dgrijalva/jwt-go.v3 v3.2.0
)

//replace github.com/ugorji/go v1.1.4 => github.com/ugorji/go/codec v0.0.0-20190204201341-e444a5086c43
