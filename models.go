package usermanager

import "time"

type ApiLogin struct {
	Email    string `form:"email" json:"email" binding:"required"`
	Password string `form:"password" json:"password" binding:"required"`
}

type ApiRefresh struct {
	Token    string `json:"token" binding:"required"`
}

type ApiReqRegister struct {
	Email    string `form:"email" json:"email" binding:"required"`
	Password string `form:"password" json:"password" binding:"required"`

	DisplayName string `form:"displayName" json:"displayName"`
}

type ApiResponse struct {
	Payload interface{} `json:"payload,omitempty"`
	Message string      `json:"msg,omitempty"`
}

type LoginToken struct {
	LoginToken   string `json:"loginToken"`
	RefreshToken string `json:"refreshToken,omitempty"`
}

type User struct {
	ID uint `json:"id" gorm:"primary_key"`

	PasswordHash string `json:"-" gorm:"type:varchar(100);not null"`
	Email        string `json:"email" gorm:"type:varchar(255);unique_index"`

	DisplayName string  `json:"displayName" gorm:"type:varchar(255)"`
	Roles       []*Role `json:"roles" gorm:"many2many:user_roles;"`
}

type RefreshToken struct {
	ID          uint      `json:"-" gorm:"primary_key"`
	Token       string    `json:"token" gorm:"type:varchar(64);unique_index"`
	Created     time.Time `json:"created"`
	Used		time.Time `json:"used"`
	Description string    `json:"desc"`

	User   *User `json:"-" gorm:"foreignkey:userid"`
	UserID uint  `json:"-"`
}

func (u *User) HasRole(roleName string) bool {
	for _, role := range u.Roles {
		if role.Name == roleName {
			return true
		}
	}

	return false
}

type Role struct {
	ID    uint    `json:"-" gorm:"primary_key"`
	Name  string  `json:"name" gorm:"type:varchar(255);unique_index"`
	Users []*User `json:"-" gorm:"many2many:user_roles;"`
}
