package usermanager

import (
	"github.com/jinzhu/gorm"
	"github.com/rs/zerolog/log"
	"golang.org/x/crypto/bcrypt"
	"time"
)

const (
	DEFAULT_EMAIL       = "admin@example.org"
	DEFAULT_PASSWORD    = "admin"
	DEFAULT_DISPLAYNAME = "Administrator"
	DEFAULT_ADMIN_ROLE  = "ROLE_ADMIN"

	BCRYPT_COST = 8
)

type UserManager struct {
	options *Options
	db      *gorm.DB
}

func (um *UserManager) Create(email string, password string, displayName string) (*User, error) {
	user := User{
		DisplayName: displayName,
		Email:       email,
	}

	passwordHash, err := bcrypt.GenerateFromPassword([]byte(password), BCRYPT_COST)
	if err != nil {
		return nil, err
	}

	user.PasswordHash = string(passwordHash)

	return &user, um.db.Create(&user).Error
}

func (um *UserManager) CreateRole(roleStr string) (*Role, error) {
	role := Role{
		Name: roleStr,
	}

	return &role, um.db.Create(&role).Error
}

func (um *UserManager) AppendRole(user *User, role *Role) error {
	return um.db.Model(user).Association("Roles").Append(role).Error
}

func (um *UserManager) Get(id int) (*User, error) {
	var user User

	err := um.db.Preload("Roles").Where("id = ?", id).Take(&user).Error
	return &user, err
}

func (um *UserManager) GetByEmail(email string) (*User, error) {
	var user User

	err := um.db.Where("email = ?", email).Take(&user).Error
	return &user, err
}

func (um *UserManager) GetRefreshToken(token string) (*RefreshToken, error) {
	var refreshToken RefreshToken

	err := um.db.Where("token = ?", token).Take(&refreshToken).Error
	if err != nil {
		return nil, err
	}

	var user User
	err = um.db.Where("id = ?", refreshToken.UserID).Take(&user).Error
	refreshToken.User = &user;

	return &refreshToken, err
}

func (um *UserManager) createRefreshToken(description string, user *User) (*RefreshToken, error) {
	refreshToken := &RefreshToken{
		User:        user,
		Created:     time.Now(),
		Used:        time.Now(),
		Token:       generateRandomRefreshToken(),
		Description: description,
		UserID:      user.ID,
	}

	if err := um.db.Create(refreshToken).Error; err != nil {
		return nil, err
	}

	return refreshToken, nil
}

func (um *UserManager) GetAuthMiddleware() *AuthMiddleware {
	return &AuthMiddleware{
		realm:            um.options.realm,
		key:              []byte(um.options.sessionKey),
		signingAlgorithm: um.options.signingAlgorithm,
		timeout:          um.options.timeout,
		userManager:      um,
	}
}

func (um *UserManager) GetGinHandler() *GinHandler {
	return &GinHandler{
		userManager:    um,
		authMiddleware: um.GetAuthMiddleware(),
	}
}

func New(db *gorm.DB, options *Options) *UserManager {
	um := &UserManager{
		options: options,
		db:      db,
	}

	if !db.HasTable(&User{}) {
		autoMigrate(db)

		initUserTable(um)
	} else {
		autoMigrate(db)
	}

	if options.enableRefreshToken {
		db.AutoMigrate(&RefreshToken{})
	}

	return um
}

func initUserTable(um *UserManager) {
	log.Info().Msg("Creating default admin role...")
	role, err := um.CreateRole(DEFAULT_ADMIN_ROLE)
	if err != nil {
		log.Error().Msgf("Error creating default admin role: %s", err)
	}

	log.Info().Msg("Creating default user...")
	user, err := um.Create(DEFAULT_EMAIL, DEFAULT_PASSWORD, DEFAULT_DISPLAYNAME)
	if err != nil {
		log.Error().Msgf("Error creating default user: %s", err)
	}

	log.Info().Msg("Appending default admin role to default user...")
	err = um.AppendRole(user, role)
	if err != nil {
		log.Error().Msgf("Error appending default admin role to default user: %s", err)
	}
}

func autoMigrate(db *gorm.DB) {
	db.AutoMigrate(
		&User{},
		&Role{},
	)
}
