package usermanager

import (
	"time"

	"gopkg.in/dgrijalva/jwt-go.v3"
)

const (
	defaultRealm = "usermanager"
)

type Options struct {
	realm string

	// token config
	sessionKey       string
	signingAlgorithm *jwt.SigningMethodHMAC
	timeout          time.Duration

	enableRefreshToken bool
}

func (o *Options) SetRealm(realm string) *Options {
	o.realm = realm

	return o
}

func (o *Options) SetSessionKey(sessionKey string) *Options {
	o.sessionKey = sessionKey

	return o
}

func (o *Options) SetSigningAlgorithm(signingAlgorithm *jwt.SigningMethodHMAC) *Options {
	o.signingAlgorithm = signingAlgorithm

	return o
}

func (o *Options) SetTimeout(timeout time.Duration) *Options {
	o.timeout = timeout

	return o
}

func (o *Options) SetEnableRefreshToken(enableRefreshToken bool) *Options {
	o.enableRefreshToken = enableRefreshToken

	return o
}

func Default() *Options {
	return &Options{
		realm:              defaultRealm,
		sessionKey:         generateRandomString(48),
		signingAlgorithm:   jwt.SigningMethodHS512,
		timeout:            time.Hour * 24,
		enableRefreshToken: false,
	}
}
