package usermanager

import (
	"github.com/nouney/randomstring"
	"github.com/rs/zerolog/log"
)

const (
	randomCharset      = randomstring.CharsetAlphaLow + randomstring.CharsetAlphaUp + randomstring.CharsetNum + "+*%&/(){}[]!$.:-_,;<>=°#"
	randomCharsetToken = randomstring.CharsetAlphaLow + randomstring.CharsetAlphaUp + randomstring.CharsetNum
)

func generateRandomString(length int) string {
	rsg, err := randomstring.NewGenerator(randomCharset)
	if err != nil {
		log.Fatal().Msgf("Error creating random string: %s", err)
	}

	return rsg.Generate(length)
}

func generateRandomRefreshToken() string {
	rsg, err := randomstring.NewGenerator(randomCharsetToken)
	if err != nil {
		log.Fatal().Msgf("Error creating random string: %s", err)
	}

	return "token-" + rsg.Generate(58)
}
